package com.escodro.alkaa.kaspresso.matcher

import android.view.View
import android.widget.CompoundButton
import androidx.annotation.ColorRes
import com.agoda.kakao.common.utilities.getResourceColor
import org.hamcrest.Description
import org.hamcrest.TypeSafeMatcher

/**
 * Матчер, который проверяет соответствие tint с передаваемым expectedTintColorId
 *
 * @param [expectedTintColorId] id цвета, с которым необходимо сравнить текущий tint
 */
class TintMatcher(
    @ColorRes private val expectedTintColorId: Int
) : TypeSafeMatcher<View>(View::class.java) {

    override fun describeTo(description: Description?) {
        description?.appendText("Цвет $expectedTintColorId не совпадает с tint")
    }

    override fun matchesSafely(item: View?): Boolean {
        if (item == null) return false

        return when (item) {
            is CompoundButton -> {
                val tintColor = item.buttonTintList?.defaultColor
                val myColor = getResourceColor(expectedTintColorId)

                tintColor == myColor
            }

            else -> {
                val tintColor = item.backgroundTintList?.defaultColor
                val myColor = getResourceColor(expectedTintColorId)

                tintColor == myColor
            }
        }
    }
}
