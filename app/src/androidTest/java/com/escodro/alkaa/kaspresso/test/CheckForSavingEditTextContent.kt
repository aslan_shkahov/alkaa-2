package com.escodro.alkaa.kaspresso.test

import com.escodro.alkaa.kaspresso.base.BaseTestCase
import com.escodro.alkaa.kaspresso.scenario.CreateTaskScenario
import com.escodro.alkaa.kaspresso.screen.TaskDetailScreen
import com.escodro.alkaa.kaspresso.screen.TaskListScreen
import org.junit.Test

class CheckForSavingEditTextContent : BaseTestCase() {

    companion object {
        private const val TASK_NAME = "taskName"
    }

    /**
     * Тест-кейс на проверку сохранения строки, которую пользователь ввёл
     * в качестве названия задачи, при выходе из приложения,
     * но не при его полном закрытии
     */
    @Test
    fun checkForSavingTaskEditTextContent() = launch().run {
        step(
            "Вписываем строку '$TASK_NAME'" +
                    " по поле ввода названия задачи"
        ) {
            TaskListScreen {
                taskRecyclerView.firstChild<TaskListScreen.AddItem> {
                    taskEditText.replaceText(TASK_NAME)
                }
            }
        }

        step("Сворачиваем приложение и заново открываем его из недавних") {
            device.uiDevice.pressHome()
            device.apps.openRecent("Alkaa")
        }

        step(
            "Проверяем сохранилась ли введённая строка '$TASK_NAME'" +
                    " в поле ввода названия задачи"
        ) {
            TaskListScreen {
                taskRecyclerView.firstChild<TaskListScreen.AddItem> {
                    taskEditText {
                        hasText(TASK_NAME)
                        isDisplayed()
                    }
                }
            }
        }
    }

    /**
     * Тест-кейс на проверку сохранения строки, которую пользователь ввёл
     * в качестве описания задачи, при выходе из приложения,
     * но не при его полном закрытии
     */
    @Test
    fun checkForSavingDescriptionEditTextContent() = launch().run {
        val taskDescription = "Some description"

        scenario(CreateTaskScenario(TASK_NAME))

        step("Нажимаем на задачу '$TASK_NAME'") {
            TaskListScreen {
                taskRecyclerView.firstChild<TaskListScreen.TaskItem> {
                    itemTaskDescription.click()
                }
            }
        }

        step(
            "Вписываем текст '$taskDescription' в поле" +
                    " с описанием задачи '$TASK_NAME'"
        ) {
            TaskDetailScreen {
                taskDetailDescription.replaceText(taskDescription)
            }
        }

        step("Сворачиваем приложение и заново открываем его из недавних") {
            device.uiDevice.pressHome()
            device.apps.openRecent("Alkaa")
        }

        step(
            "Проверяем сохранилась ли введённая строка '$taskDescription'" +
                    " в поле ввода с описанием задачи '$TASK_NAME'"
        ) {
            TaskDetailScreen {
                taskDetailDescription.hasText(taskDescription)
            }
        }
    }
}
