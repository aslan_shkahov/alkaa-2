package com.escodro.alkaa.kaspresso.screen

import android.content.res.Resources
import android.view.View
import androidx.fragment.app.Fragment
import com.agoda.kakao.check.KCheckBox
import com.agoda.kakao.common.views.KView
import com.agoda.kakao.edit.KEditText
import com.agoda.kakao.image.KImageView
import com.agoda.kakao.recycler.KRecyclerItem
import com.agoda.kakao.recycler.KRecyclerView
import com.agoda.kakao.text.KButton
import com.agoda.kakao.text.KTextView
import com.escodro.alkaa.R
import com.escodro.alkaa.kaspresso.base.BaseScreen
import com.escodro.alkaa.kaspresso.elements.KNavToolbar
import com.escodro.task.presentation.list.TaskListFragment
import org.hamcrest.Matcher
import kotlin.reflect.KClass

object TaskListScreen : BaseScreen<TaskListScreen>() {
    override val layout: Int
        get() = R.layout.activity_main
    override val fragment: KClass<out Fragment>
        get() = TaskListFragment::class

    //Toolbar элементы
    val mainToolbar = KNavToolbar { withId(R.id.toolbar_main_toolbar) }
    val toolbarTitle = KTextView { withId(R.id.toolbar_title) }

    val taskListEmpty = KTextView { withId(R.id.textview_tasklist_empty) }

    val taskRecyclerView = KRecyclerView(
        builder = { withId(R.id.recyclerview_tasklist_list) },
        itemTypeBuilder = {
            itemType(::TaskItem)
            itemType(::AddItem)
        }
    )

    val undoDialogButton = KButton { withId(R.id.snackbar_action) }

    /**
     * Метод, который возвращает кнопку категории из Navigation Draw по названию категории
     *
     * @param [categoryName] название категории, по которому необходимо вернуть кнопку
     */
    fun getCategory(categoryName: String): KButton {
        return KButton { withText(categoryName) }
    }

    /**
     * Метод, который возвращает кнопку удаления задачи
     *
     * @param [resources] ресурсы, переменная необходима для
     * вызова метода getStringArray, лучше всего получать её
     * через targetContext, а не через обычный context
     */
    fun getRemoveButton(resources: Resources): KButton {
        val buttonText = resources.getStringArray(R.array.task_dialog_options)[0] ?: ""

        return KButton { withText(buttonText) }
    }

    class TaskItem(matcher: Matcher<View>) : KRecyclerItem<TaskItem>(matcher) {

        val itemTaskBackground = KView(matcher) { withId(R.id.cardview_itemtask_background) }
        val itemTaskColor = KView(matcher) { withId(R.id.view_itemtask_color) }
        val itemTaskCheckBox = KCheckBox(matcher) { withId(R.id.checkbox_itemtask_completed) }
        val itemTaskDescription = KTextView(matcher) { withId(R.id.textview_itemtask_description) }
        val itemTaskAlarm = KTextView(matcher) { withId(R.id.textview_itemtask_alarm) }
        val itemTaskRepeatingLogo =
            KImageView(matcher) { withId(R.id.imageview_itemtask_repeating) }
    }

    class AddItem(matcher: Matcher<View>) : KRecyclerItem<TaskItem>(matcher) {

        val taskEditText = KEditText { withId(R.id.edittext_itemadd_description) }
        val itemAddBackground = KView { withId(R.id.cardview_itemadd_background) }
        val itemPlusIcon = KImageView { withId(R.id.imageview_itemadd_completed) }
    }

}
