package com.escodro.alkaa.kaspresso.scenario

import com.escodro.alkaa.kaspresso.screen.TaskListScreen
import com.kaspersky.kaspresso.testcases.api.scenario.Scenario
import com.kaspersky.kaspresso.testcases.core.testcontext.TestContext
import org.junit.Assert

/**
 * Сценарий по проверке отсутствия задачи с указанным названием
 *
 * @param [taskName] название задачи, наличие которой необходимо проверить
 * @param [prevRecyclerViewSize] количество элементов в recyclerView до
 * возможного добавления элемента
 */
class CheckNoTaskScenario(
    taskName: String,
    prevRecyclerViewSize: Int = 1
) : Scenario() {

    override val steps: TestContext<Unit>.() -> Unit = {
        step("Проверяем наличие задачи '$taskName'") {
            TaskListScreen {

                Assert.assertTrue(
                    "Размер списка с задачами не равен" +
                            " предыдущему размеру: '$prevRecyclerViewSize'",
                    prevRecyclerViewSize == taskRecyclerView.getSize()
                )

                if (prevRecyclerViewSize != 1) {
                    taskRecyclerView.children<TaskListScreen.TaskItem> {
                        itemTaskDescription.hasNoText(taskName)
                    }
                }
            }
        }
    }
}
