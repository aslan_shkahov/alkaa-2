package com.escodro.alkaa.kaspresso.test

import androidx.test.espresso.matcher.ViewMatchers
import com.agoda.kakao.common.utilities.getResourceString
import com.escodro.alkaa.R
import com.escodro.alkaa.kaspresso.base.BaseTestCase
import com.escodro.alkaa.kaspresso.extension.checkShapeViewColor
import com.escodro.alkaa.kaspresso.scenario.CreateCategoryScenario
import com.escodro.alkaa.kaspresso.screen.CategoriesScreen
import com.escodro.alkaa.kaspresso.screen.CategoryScreen
import org.junit.Assert
import org.junit.Test

class EditCategory : BaseTestCase() {

    /**
     * Тест-кейс, который проверяет возможность изменения
     * названия категории на другое простое название
     */
    @Test
    fun changeToTheSimpleName() = launch().run {
        val categoryName = "categoryName"
        val newCategoryName = "newCategoryName"
        var categoriesRecyclerSize = 0

        scenario(
            CreateCategoryScenario(categoryName, CategoryScreen.greenRadioButton)
        )

        step(
            "Нажимаем на кнопку открытия меню" +
                    " для взаимодействия с категорией '$categoryName'"
        ) {
            CategoriesScreen {
                categoriesRecyclerSize = recyclerViewCategoryList.getSize()

                recyclerViewCategoryList.lastChild<CategoriesScreen.CategoryItem> {
                    itemCategoryOptions.click()
                }
            }
        }

        step("Нажимаем на кнопку изменения задачи '$categoryName'") {
            CategoriesScreen {
                editCategoryItem.click()
            }
        }

        step(
            "Вставляем новое название '$newCategoryName'" +
                    " для категории '$categoryName'"
        ) {
            CategoryScreen {
                categoryEditText.replaceText(newCategoryName)
            }
        }

        step("Нажимаем на кнопку сохранение изменений категории") {
            CategoryScreen {
                addCategoryButton.click()
            }
        }

        step("Проверяем наличие категории '$newCategoryName' в списке категорий") {
            CategoriesScreen {
                Assert.assertTrue(
                    "Размер списка с категориями не равен" +
                            " предыдущему размеру: '$categoriesRecyclerSize'",
                    categoriesRecyclerSize == recyclerViewCategoryList.getSize()
                )

                recyclerViewCategoryList.lastChild<CategoriesScreen.CategoryItem> {
                    itemCategoryDescription {
                        isDisplayed()
                        hasText(newCategoryName)
                    }
                }
            }
        }
    }

    /**
     * Тест-кейс, который проверяет возможность
     * изменения названия категории на пустую строку
     */
    @Test
    fun changeToTheEmptyName() = launch().run {
        val categoryName = "categoryName"
        val emptyCategoryName = ""
        var categoriesRecyclerSize = 0

        scenario(
            CreateCategoryScenario(categoryName, CategoryScreen.greenRadioButton)
        )

        step(
            "Нажимаем на кнопку открытия меню" +
                    " для взаимодействия с категорией '$categoryName'"
        ) {
            CategoriesScreen {
                categoriesRecyclerSize = recyclerViewCategoryList.getSize()

                recyclerViewCategoryList.lastChild<CategoriesScreen.CategoryItem> {
                    itemCategoryOptions.click()
                }
            }
        }

        step("Нажимаем на кнопку изменения задачи '$categoryName'") {
            CategoriesScreen {
                editCategoryItem.click()
            }
        }

        step(
            "Вставляем новое название '$emptyCategoryName'" +
                    " для категории '$categoryName'"
        ) {
            CategoryScreen {
                categoryEditText.replaceText(emptyCategoryName)
            }
        }

        step("Нажимаем на кнопку сохранение изменений категории") {
            CategoryScreen {
                addCategoryButton.click()
            }
        }

        step(
            "Проверяем отображение сообщения об ошибке" +
                    " о пустом поле у поля ввода"
        ) {
            CategoryScreen {
                categoryEditText {
                    matches {
                        ViewMatchers.hasErrorText(getResourceString(R.string.category_detail_error_empty))
                    }
                }
            }
        }

        step("Возвращаемся на экран со списком категорий") {
            CategoryScreen {
                mainToolbar.navigationButton.click()
            }
        }

        step(
            "Проверяем отсутствие категории '$categoryName' в списке категорий"
        ) {
            CategoriesScreen {
                Assert.assertTrue(
                    "Размер списка с категориями не равен" +
                            " предыдущему размеру: '$categoriesRecyclerSize'",
                    categoriesRecyclerSize == recyclerViewCategoryList.getSize()
                )

                recyclerViewCategoryList.lastChild<CategoriesScreen.CategoryItem> {
                    itemCategoryDescription {
                        isDisplayed()
                        hasText(categoryName)
                    }
                }
            }
        }
    }

    /**
     * Тест-кейс, который проверяет возможность изменения
     * названия категории на строку с пробелами
     */
    @Test
    fun changeToTheWhitespaceName() = launch().run {
        val categoryName = "categoryName"
        val newCategoryName = " "
        var categoriesRecyclerSize = 0

        scenario(
            CreateCategoryScenario(categoryName, CategoryScreen.greenRadioButton)
        )

        step(
            "Нажимаем на кнопку открытия меню" +
                    " для взаимодействия с категорией '$categoryName'"
        ) {
            CategoriesScreen {
                categoriesRecyclerSize = recyclerViewCategoryList.getSize()

                recyclerViewCategoryList.lastChild<CategoriesScreen.CategoryItem> {
                    itemCategoryOptions.click()
                }
            }
        }

        step("Нажимаем на кнопку изменения задачи '$categoryName'") {
            CategoriesScreen {
                editCategoryItem.click()
            }
        }

        step(
            "Вставляем новое название '$newCategoryName'" +
                    " для категории '$categoryName'"
        ) {
            CategoryScreen {
                categoryEditText.replaceText(newCategoryName)
            }
        }

        step("Нажимаем на кнопку сохранение изменений категории") {
            CategoryScreen {
                addCategoryButton.click()
            }
        }

        step(
            "Проверяем отображение сообщения об ошибке" +
                    " о пустом поле у поля ввода"
        ) {
            CategoryScreen {
                categoryEditText {
                    matches {
                        ViewMatchers.hasErrorText(getResourceString(R.string.category_detail_error_empty))
                    }
                }
            }
        }

        step("Возвращаемся на экран со списком категорий") {
            CategoryScreen {
                mainToolbar.navigationButton.click()
            }
        }

        step(
            "Проверяем отсутствие категории '$categoryName' в списке категорий"
        ) {
            CategoriesScreen {
                Assert.assertTrue(
                    "Размер списка с категориями не равен" +
                            " предыдущему размеру: '$categoriesRecyclerSize'",
                    categoriesRecyclerSize == recyclerViewCategoryList.getSize()
                )

                recyclerViewCategoryList.lastChild<CategoriesScreen.CategoryItem> {
                    itemCategoryDescription {
                        isDisplayed()
                        hasText(categoryName)
                    }
                }
            }
        }
    }

    /**
     * Тест-кейс, который проверяет возможность изменения
     * названия категории на строку со специальными символами
     */
    @Test
    fun changeToTheNameWithSpecialCharacters() = launch().run {
        val categoryName = "categoryName"
        val newCategoryName = "{123?.<>$/\\=+*}😀"
        var categoriesRecyclerSize = 0

        scenario(
            CreateCategoryScenario(categoryName, CategoryScreen.greenRadioButton)
        )

        step(
            "Нажимаем на кнопку открытия меню" +
                    " для взаимодействия с категорией '$categoryName'"
        ) {
            CategoriesScreen {
                categoriesRecyclerSize = recyclerViewCategoryList.getSize()

                recyclerViewCategoryList.lastChild<CategoriesScreen.CategoryItem> {
                    itemCategoryOptions.click()
                }
            }
        }

        step("Нажимаем на кнопку изменения задачи '$categoryName'") {
            CategoriesScreen {
                editCategoryItem.click()
            }
        }

        step(
            "Вставляем новое название '$newCategoryName'" +
                    " для категории '$categoryName'"
        ) {
            CategoryScreen {
                categoryEditText.replaceText(newCategoryName)
            }
        }

        step("Нажимаем на кнопку сохранение изменений категории") {
            CategoryScreen {
                addCategoryButton.click()
            }
        }

        step("Проверяем наличие категории '$newCategoryName' в списке категорий") {
            CategoriesScreen {
                Assert.assertTrue(
                    "Размер списка с категориями не равен" +
                            " предыдущему размеру: '$categoriesRecyclerSize'",
                    categoriesRecyclerSize == recyclerViewCategoryList.getSize()
                )

                recyclerViewCategoryList.lastChild<CategoriesScreen.CategoryItem> {
                    itemCategoryDescription {
                        isDisplayed()
                        hasText(newCategoryName)
                    }
                }
            }
        }
    }

    /**
     * Тест-кейс, который проверяет возможность изменения цвета категории
     */
    @Test
    fun changeColor() = launch().run {
        val categoryName = "categoryName"
        var categoriesRecyclerSize = 0

        scenario(
            CreateCategoryScenario(categoryName, CategoryScreen.greenRadioButton)
        )

        step(
            "Нажимаем на кнопку открытия меню" +
                    " для взаимодействия с категорией '$categoryName'"
        ) {
            CategoriesScreen {
                categoriesRecyclerSize = recyclerViewCategoryList.getSize()

                recyclerViewCategoryList.lastChild<CategoriesScreen.CategoryItem> {
                    itemCategoryOptions.click()
                }
            }
        }

        step("Нажимаем на кнопку изменения задачи '$categoryName'") {
            CategoriesScreen {
                editCategoryItem.click()
            }
        }

        step("Изменяем цвет категории на красный") {
            CategoryScreen {
                redRadioButton.click()
            }
        }

        step("Нажимаем на кнопку сохранение изменений категории") {
            CategoryScreen {
                addCategoryButton.click()
            }
        }

        step(
            "Проверяем наличие категории '$categoryName' в списке категорий"
        ) {
            CategoriesScreen {
                Assert.assertTrue(
                    "Размер списка с категориями не равен" +
                            " предыдущему размеру: '$categoriesRecyclerSize'",
                    categoriesRecyclerSize == recyclerViewCategoryList.getSize()
                )

                recyclerViewCategoryList.lastChild<CategoriesScreen.CategoryItem> {
                    itemCategoryColor.checkShapeViewColor(R.color.red)
                }
            }
        }
    }
}
