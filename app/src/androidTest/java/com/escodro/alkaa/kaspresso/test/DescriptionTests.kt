package com.escodro.alkaa.kaspresso.test

import com.escodro.alkaa.kaspresso.base.BaseTestCase
import com.escodro.alkaa.kaspresso.scenario.CreateTaskScenario
import com.escodro.alkaa.kaspresso.screen.TaskDetailScreen
import com.escodro.alkaa.kaspresso.screen.TaskListScreen
import com.kaspersky.kaspresso.testcases.core.testcontext.TestContext
import org.junit.Test

class DescriptionTests : BaseTestCase() {

    companion object {
        private const val TASK_NAME = "taskName"
        private const val TASK_DESCRIPTION = "Some description"
    }

    /**
     * Тест-кейс, который проверяет сохраняется ли описание задачи
     * при выходе с экрана содержащего подробную информацию о задаче
     * и возврата на него
     */
    @Test
    fun addDescription() = launch().run {
        writeDescription()

        step(
            "Проверяем сохранился ли вписанный текст '$TASK_DESCRIPTION'" +
                    " в поле с описанием задачи '$TASK_NAME'"
        ) {
            TaskDetailScreen {
                taskDetailDescription {
                    isDisplayed()
                    hasText(TASK_DESCRIPTION)
                }
            }
        }
    }

    /**
     * Тест-кейс, который проверяет возможность изменить
     * описание задачи
     */
    @Test
    fun changeDescription() = launch().run {
        val newTaskDescriptionTests = "Some description 2.0"

        writeDescription()

        step("Меняем текст описания задачи на '$newTaskDescriptionTests'") {
            TaskDetailScreen {
                taskDetailDescription.replaceText(newTaskDescriptionTests)
            }
        }

        step("Возвращаемся обратно на экран AllTasks") {
            TaskDetailScreen {
                mainToolbar.navigationButton.click()
            }
        }

        step("Нажимаем на задачу '$TASK_NAME'") {
            TaskListScreen {
                taskRecyclerView.firstChild<TaskListScreen.TaskItem> {
                    itemTaskDescription.click()
                }
            }
        }

        step(
            "Проверяем сохранился ли вписанный текст '$newTaskDescriptionTests'" +
                    " в поле с описанием задачи '$TASK_NAME'"
        ) {
            TaskDetailScreen {
                taskDetailDescription {
                    isDisplayed()
                    hasText(newTaskDescriptionTests)
                }
            }
        }
    }

    private fun TestContext<Unit>.writeDescription() {
        scenario(CreateTaskScenario(TASK_NAME))

        step("Нажимаем на задачу '$TASK_NAME'") {
            TaskListScreen {
                taskRecyclerView.firstChild<TaskListScreen.TaskItem> {
                    itemTaskDescription.click()
                }
            }
        }

        step(
            "Заменяем текст в поле с описанием на '$TASK_DESCRIPTION'" +
                    "у задачи '$TASK_NAME'"
        ) {
            TaskDetailScreen {
                taskDetailDescription.replaceText(TASK_DESCRIPTION)
            }
        }

        step("Возвращаемся обратно на экран AllTasks") {
            TaskDetailScreen {
                mainToolbar.navigationButton.click()
            }
        }

        step("Нажимаем на задачу '$TASK_NAME'") {
            TaskListScreen {
                taskRecyclerView.firstChild<TaskListScreen.TaskItem> {
                    itemTaskDescription.click()
                }
            }
        }
    }
}
