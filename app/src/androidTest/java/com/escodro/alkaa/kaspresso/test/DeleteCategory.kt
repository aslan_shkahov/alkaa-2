package com.escodro.alkaa.kaspresso.test

import com.agoda.kakao.common.utilities.getResourceString
import com.escodro.alkaa.R
import com.escodro.alkaa.kaspresso.base.BaseTestCase
import com.escodro.alkaa.kaspresso.scenario.CreateCategoryScenario
import com.escodro.alkaa.kaspresso.scenario.CreateTaskScenario
import com.escodro.alkaa.kaspresso.scenario.MoveBetweenTabsScenario
import com.escodro.alkaa.kaspresso.scenario.PressFirstTaskCategoryScenario
import com.escodro.alkaa.kaspresso.screen.CategoriesScreen
import com.escodro.alkaa.kaspresso.screen.CategoryScreen
import com.escodro.alkaa.kaspresso.screen.TaskListScreen
import org.junit.Test

class DeleteCategory : BaseTestCase() {

    /**
     * Тест-кейс, который проверяет возможность удалить пустую категорию
     */
    @Test
    fun deleteEmptyCategory() = launch().run {
        val categoryName = "categoryName"

        scenario(
            CreateCategoryScenario(
                categoryName,
                CategoryScreen.blueRadioButton
            )
        )

        step(
            "Нажимаем на кнопку открытия меню" +
                    " для взаимодействия с категорией '$categoryName'"
        ) {
            CategoriesScreen {
                recyclerViewCategoryList.lastChild<CategoriesScreen.CategoryItem> {
                    itemCategoryOptions.click()
                }
            }
        }

        step("Открываем меню для взаимодействия с задачей '$categoryName'") {
            CategoriesScreen {
                removeCategoryItem.click()
            }
        }

        step("Подтверждаем удаление категории '$categoryName'") {
            CategoriesScreen {
                removeDialog.positiveButton.click()
            }
        }

        step("Проверяем отсутствие категории '$categoryName' в списке категорий") {
            CategoriesScreen {
                recyclerViewCategoryList.lastChild<CategoriesScreen.CategoryItem> {
                    itemCategoryDescription.hasNoText(categoryName)
                }
            }
        }
    }

    /**
     * Тест-кейс, который проверяет возможность
     * удаления категории с задачами вместе с задачами
     */
    @Test
    fun deleteCategoryWithTasks() = launch().run {
        val firstTaskName = "firstTaskName"
        val secondTaskName = "secondTaskName"
        val categoryName = "categoryName"

        scenario(CreateTaskScenario(firstTaskName))

        scenario(CreateTaskScenario(secondTaskName))

        scenario(CreateCategoryScenario(categoryName, CategoryScreen.greenRadioButton))

        step("Возвращаемся на экран с задачами") {
            CategoriesScreen {
                mainToolbar.navigationButton.click()
            }
        }

        scenario(PressFirstTaskCategoryScenario(categoryName))

        scenario(
            MoveBetweenTabsScenario(
                getResourceString(R.string.drawer_menu_manage_categories)
            )
        )

        step(
            "Нажимаем на кнопку открытия меню" +
                    " для взаимодействия с категорией '$categoryName'"
        ) {
            CategoriesScreen {
                recyclerViewCategoryList.lastChild<CategoriesScreen.CategoryItem> {
                    itemCategoryOptions.click()
                }
            }
        }

        step("Открываем меню для взаимодействия с задачей '$categoryName'") {
            CategoriesScreen {
                removeCategoryItem.click()
            }
        }

        step("Подтверждаем удаление категории '$categoryName'") {
            CategoriesScreen {
                removeDialog.positiveButton.click()
            }
        }

        step("Проверяем отсутствие категории '$categoryName' в списке категорий") {
            CategoriesScreen {
                recyclerViewCategoryList.lastChild<CategoriesScreen.CategoryItem> {
                    itemCategoryDescription.hasNoText(categoryName)
                }
            }
        }

        step("Возвращаемся на экран с задачами") {
            CategoriesScreen {
                mainToolbar.navigationButton.click()
            }
        }

        step(
            "Проверяем наличие второй задачи, которая не была добавлена в категорию," +
                    " на экране AllTasks"
        ) {
            TaskListScreen {
                taskRecyclerView.firstChild<TaskListScreen.TaskItem> {
                    itemTaskDescription {
                        isDisplayed()
                        hasText(secondTaskName)
                    }
                }
            }
        }
    }
}
