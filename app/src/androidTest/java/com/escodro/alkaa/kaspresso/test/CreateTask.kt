package com.escodro.alkaa.kaspresso.test

import com.agoda.kakao.screen.Screen
import com.escodro.alkaa.kaspresso.base.BaseTestCase
import com.escodro.alkaa.kaspresso.scenario.CheckNoTaskScenario
import com.escodro.alkaa.kaspresso.scenario.CheckTaskScenario
import com.escodro.alkaa.kaspresso.scenario.CreateTaskScenario
import org.junit.Test

class CreateTask : BaseTestCase() {

    /**
     * Тест-кейс с созданием задачи с простым названием
     */
    @Test
    fun simpleTask() = launch().run {
        val taskName = "Hello"

        scenario(CreateTaskScenario(taskName))

        Screen.idle()
        scenario(CheckTaskScenario(taskName))
    }

    /**
     * Тест-кейс с созданием задачи с названием содержащим специальные символы
     */
    @Test
    fun taskWithSpecialCharacters() = launch().run {
        val taskName = "{123?.<>$/\\=+*}😀"

        scenario(CreateTaskScenario(taskName))

        Screen.idle()
        scenario(CheckTaskScenario(taskName))
    }

    /**
     * Тест-кейс с созданием задачи с названием содержащим только пробелы
     */
    @Test
    fun whitespaceTask() = launch().run {
        scenario(CreateTaskScenario(taskName = " "))

        Screen.idle()
        scenario(CheckNoTaskScenario(taskName = " "))
    }

    /**
     * Тест-кейс с созданием задачи с пустым названием
     */
    @Test
    fun emptyTask() = launch().run {
        scenario(CreateTaskScenario(taskName = ""))

        Screen.idle()
        scenario(CheckNoTaskScenario(taskName = ""))
    }
}
