package com.escodro.alkaa.kaspresso.screen

import android.view.View
import androidx.fragment.app.Fragment
import com.agoda.kakao.recycler.KRecyclerItem
import com.agoda.kakao.recycler.KRecyclerView
import com.agoda.kakao.text.KTextView
import com.escodro.alkaa.R
import com.escodro.alkaa.kaspresso.base.BaseScreen
import org.hamcrest.Matcher
import kotlin.reflect.KClass

object PreferencesScreen : BaseScreen<PreferencesScreen>() {
    override val layout: Int
        get() = TODO("Not yet implemented")
    override val fragment: KClass<out Fragment>
        get() = TODO("Not yet implemented")

    val recyclerView = KRecyclerView(
        builder = { withId(R.id.recycler_view) },
        itemTypeBuilder = {
            itemType(::PreferencesItem)
        }
    )

    class PreferencesItem(matcher: Matcher<View>) : KRecyclerItem<PreferencesItem>(matcher) {

        val itemTextView = KTextView { withText(R.string.preference_item_about) }
    }
}