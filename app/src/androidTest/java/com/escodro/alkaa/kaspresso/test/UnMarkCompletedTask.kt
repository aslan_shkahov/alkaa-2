package com.escodro.alkaa.kaspresso.test

import com.agoda.kakao.common.utilities.getResourceString
import com.escodro.alkaa.R
import com.escodro.alkaa.kaspresso.base.BaseTestCase
import com.escodro.alkaa.kaspresso.scenario.CheckTaskScenario
import com.escodro.alkaa.kaspresso.scenario.CreateTaskScenario
import com.escodro.alkaa.kaspresso.scenario.MoveBetweenTabsScenario
import com.escodro.alkaa.kaspresso.screen.TaskDetailScreen
import com.escodro.alkaa.kaspresso.screen.TaskListScreen
import org.junit.Test

class UnMarkCompletedTask : BaseTestCase() {

    companion object {
        private const val TASK_NAME = "taskName"
    }

    /**
     * Тест-кейс, который проверяет возможность отмены выполнения задачи
     * при нажатии на checkBox на экране Completed
     */
    @Test
    fun unMarkByCheckBox() = launch().run {
        scenario(CreateTaskScenario(TASK_NAME))

        step("Ставим галочку в checkBox задачи '$TASK_NAME'") {
            TaskListScreen {
                taskRecyclerView.firstChild<TaskListScreen.TaskItem> {
                    itemTaskCheckBox.click()
                }
            }
        }

        scenario(
            MoveBetweenTabsScenario(
                getResourceString(R.string.drawer_menu_completed_tasks)
            )
        )

        step("Убираем галочку из checkBox задачи '$TASK_NAME'") {
            TaskListScreen {
                taskRecyclerView.firstChild<TaskListScreen.TaskItem> {
                    itemTaskCheckBox.click()
                }
            }
        }

        step("Проверяем, что на экране Completed задача '$TASK_NAME' отсутствует") {
            TaskListScreen {
                taskRecyclerView.isNotDisplayed()
                taskListEmpty.isVisible()
            }
        }

        scenario(
            MoveBetweenTabsScenario(
                getResourceString(R.string.drawer_menu_all_tasks)
            )
        )

        scenario(CheckTaskScenario(TASK_NAME))

    }

    /**
     * Тест-кейс, который проверяет возможность отмены выполнения задачи
     * при нажатии на галочку на экране с подробной информацией
     * о задаче
     */
    @Test
    fun unMarkTaskByDoneButton() = launch().run {
        scenario(CreateTaskScenario(TASK_NAME))

        step("Открываем экран с подробной информацией о задаче '$TASK_NAME'") {
            TaskListScreen {
                taskRecyclerView.firstChild<TaskListScreen.TaskItem> {
                    itemTaskDescription.click()
                }
            }
        }

        step(
            "Два раза нажимаем на галочку в правом верхнем углу" +
                    "на экране с подробной информацией о задаче '$TASK_NAME'"
        ) {
            TaskDetailScreen {
                mainToolbar.doneButton.doubleClick()
            }
        }

        step(
            "Проверяем отображение всплывающего окна," +
                    " которое сообщает об отмене выполнения задачи '$TASK_NAME'"
        ) {
            TaskDetailScreen
                .checkToast(R.string.task_status_mark_uncompleted)
                .isDisplayed()
        }

        step("Возвращаемся на экран AllTasks") {
            TaskDetailScreen {
                mainToolbar.navigationButton.click()
            }
        }

        scenario(CheckTaskScenario(TASK_NAME))

        scenario(
            MoveBetweenTabsScenario(
                getResourceString(R.string.drawer_menu_completed_tasks)
            )
        )

        step("Проверяем, что на экране Completed задача '$TASK_NAME' отсутствует") {
            TaskListScreen {
                taskRecyclerView.isNotDisplayed()
                taskListEmpty.isVisible()
            }
        }
    }

    /**
     * Тест-кейс, который проверяет возможность отмены выполнения задачи
     * при нажатии на кнопку Undo в появившемся диалоговом окне, после
     * нажатия на checkBox
     */
    @Test
    fun unMarkByUndoDialog() = launch().run {
        scenario(CreateTaskScenario(TASK_NAME))

        step("Ставим галочку в checkBox задачи '$TASK_NAME'") {
            TaskListScreen {
                taskRecyclerView.firstChild<TaskListScreen.TaskItem> {
                    itemTaskCheckBox.click()
                }
            }
        }

        step("Нажимаем на кнопку Undo в появившемся диалоговом окне") {
            TaskListScreen.undoDialogButton {
                isDisplayed()
                click()
            }
        }

        scenario(CheckTaskScenario(TASK_NAME))

        scenario(
            MoveBetweenTabsScenario(
                getResourceString(R.string.drawer_menu_completed_tasks)
            )
        )

        step("Проверяем, что на экране Completed задача '$TASK_NAME' отсутствует") {
            TaskListScreen {
                taskRecyclerView.isNotDisplayed()
                taskListEmpty.isVisible()
            }
        }
    }
}
