package com.escodro.alkaa.kaspresso.matcher

import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.CompoundButton
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.graphics.drawable.toBitmap
import com.agoda.kakao.common.utilities.getResourceColor
import com.agoda.kakao.common.utilities.getResourceDrawable
import org.hamcrest.Description
import org.hamcrest.TypeSafeMatcher

/**
 * Матчер, который проверяет соответствие переданного drawable и тому что находится у View
 *
 * @param [expectedDrawableId] id ресурса drawable
 * @param [expectedTintColorId] id цвета View, который должен быть на данный момент
 */
class CheckIconMatcher(
    @DrawableRes private val expectedDrawableId: Int,
    @ColorRes private val expectedTintColorId: Int
) : TypeSafeMatcher<View>(View::class.java) {

    override fun describeTo(description: Description?) {
        description?.appendText(
            "Drawable не совпадают" +
                    " DrawableResId '$expectedDrawableId', tintColorId '$expectedTintColorId'"
        )
    }

    override fun matchesSafely(item: View?): Boolean {
        if (item == null) return false

        if (item is CompoundButton) {
            val expectDrawable: Drawable = getResourceDrawable(expectedDrawableId) ?: return false

            expectDrawable.apply {
                setTintList(ColorStateList.valueOf(getResourceColor(expectedTintColorId)))
            }

            val currentDrawable = item.buttonDrawable?.current ?: return false

            val bitmap = currentDrawable.toBitmap()
            val otherBitmap = expectDrawable.toBitmap()

            return bitmap.sameAs(otherBitmap)
        }

        return false
    }
}
