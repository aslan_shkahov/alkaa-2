package com.escodro.alkaa.kaspresso.test

import com.escodro.alkaa.kaspresso.base.BaseTestCase
import com.escodro.alkaa.kaspresso.scenario.CreateTaskScenario
import com.escodro.alkaa.kaspresso.screen.TaskDetailScreen
import com.escodro.alkaa.kaspresso.screen.TaskListScreen
import com.escodro.alkaa.kaspresso.uiScreen.NotificationUiScreen
import com.escodro.alkaa.kaspresso.uiScreen.TaskDetailUiScreen

class AlarmTests : BaseTestCase() {

    /**
     * Тест-кейс, который проверяет приходит ли оповещение о запланированной задаче
     */
    //@Test
    fun setAlarm() = launch(before = {
        testLogger.i(
            "setAlarm", "Выставляем изначальную дату в" +
                    " приложении на 09.09.2009 00:00:00"
        )
        adbServer.performShell("settings put global auto_time 0")
        adbServer.performShell("date 090900002009.00")
    }).run {
        val taskName = "taskName"

        scenario(CreateTaskScenario(taskName))

        step("Нажимаем на только что созданную задачу '$taskName'") {
            TaskListScreen {
                taskRecyclerView.firstChild<TaskListScreen.TaskItem> {
                    itemTaskDescription.click()
                }
            }
        }

        step("Выставляем оповещение через 10 минут") {
            TaskDetailScreen {
                alarmButton.click()

                datePickerDialog {
                    datePicker.setDate(2009, 9, 9)
                    okButton.click()
                }

                timePickerDialog {
                    timePicker.setTime(hour = 0, minutes = 10)
                    okButton.click()
                }

                mainToolbar.navigationButton.click()
            }
        }

        step("Открываем список уведомлений") {
            device.uiDevice.openNotification()
        }

        step("Переводим дату на 9 минут 59 секунд вперёд") {
            adbServer.performShell("date 090900092009.59")
        }

        step("Нажимаем на появившееся уведомление") {
            flakySafely(120_000L) {
                NotificationUiScreen.getNotification(taskName).click()
            }
        }

        step(
            "Проверяем что приложение открылось на странице с подробной" +
                    "информацией о задаче '$taskName'"
        ) {
            TaskDetailUiScreen {
                taskDetailTitle.click()
            }
        }
    }
}
