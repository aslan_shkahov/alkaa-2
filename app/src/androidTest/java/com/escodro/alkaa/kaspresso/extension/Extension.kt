package com.escodro.alkaa.kaspresso.extension

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.test.espresso.assertion.ViewAssertions
import com.agoda.kakao.check.KCheckBox
import com.agoda.kakao.common.views.KView
import com.agoda.kakao.edit.KEditText
import com.escodro.alkaa.kaspresso.matcher.CheckIconMatcher
import com.escodro.alkaa.kaspresso.matcher.CheckShapeViewColorMatcher
import com.escodro.alkaa.kaspresso.matcher.TintMatcher

/**
 * Функция, которая предоставляет доступ к матчеру CheckIconMatcher,
 * который проверяет соотвествие текущего drawable ожидаемому
 *
 * @param [expectedDrawableId] id ресурса drawable
 * @param [expectedTintColorId] id цвета View, который должен быть на данный момент
 */
fun KCheckBox.checkIcon(
    @DrawableRes expectedDrawableId: Int,
    @ColorRes expectedTintColorId: Int
) {
    this.view.check(
        ViewAssertions.matches(
            CheckIconMatcher(
                expectedDrawableId,
                expectedTintColorId
            )
        )
    )
}

/**
 * Функция, которая предоставляет доступ к матчеру CheckShapeViewColorMatcher,
 * который проверяет соответствие переданного цвета с тем,
 * который есть на текущий момент у ShapeView
 *
 * @param [expectedTintColorId] id цвета ShapeView, который должен быть у передаваемого ShapeView
 */
fun KView.checkShapeViewColor(@ColorRes expectedTintColorId: Int) {
    this.view.check(
        ViewAssertions.matches(
            CheckShapeViewColorMatcher(expectedTintColorId)
        )
    )
}

/**
 * Функция, которая предоставляет доступ к матчеру TintMatcher,
 * который проверяет соответствие tint с передаваемым expectedTintColorId
 *
 * @param [expectedTintColorId] id цвета, с которым необходимо сравнить текущий tint
 */
fun KEditText.checkTint(@ColorRes expectedTintColorId: Int) {
    this.view.check(
        ViewAssertions.matches(
            TintMatcher(expectedTintColorId)
        )
    )
}

/**
 * Функция, которая предоставляет доступ к матчеру TintMatcher,
 * который проверяет соответствие tint с передаваемым expectedTintColorId
 *
 * @param [expectedTintColorId] id цвета, с которым необходимо сравнить текущий tint
 */
fun KCheckBox.checkTint(@ColorRes expectedTintColorId: Int) {
    this.view.check(
        ViewAssertions.matches(
            TintMatcher(expectedTintColorId)
        )
    )
}
