package com.escodro.alkaa.kaspresso.base

import android.Manifest
import android.content.Intent
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import com.escodro.alkaa.kaspresso.interseptors.AllureMapperStepInterceptor
import com.escodro.alkaa.kaspresso.interseptors.EspressoScreenshot
import com.escodro.alkaa.kaspresso.interseptors.ScreenshotStepInterceptor
import com.escodro.alkaa.presentation.MainActivity
import com.kaspersky.kaspresso.kaspresso.Kaspresso
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import com.kaspersky.kaspresso.testcases.core.sections.InitSection
import com.kaspersky.kaspresso.testcases.core.testcontext.BaseTestContext
import org.junit.Rule

open class BaseTestCase(builder: Kaspresso.Builder = defaultBuilder) : TestCase(builder) {

    /**
     * Указываем необходимые разрешения для приложения перед тестом
     */
    @get:Rule
    val runtimePermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )

    /**
     * Действия, выполняемые до запуска теста
     *
     * @param [userDataType] Пользователь из списка [UserDataType], от лица которого будет выполняться сценарий
     * @param [before] Действия, которые следует выполнить до теста
     * @param [after] Действия, которые следует выполнить после теста
     */
    protected fun launch(
        //userDataType: UserDataType? = null,
        before: BaseTestContext.() -> Unit = {},
        after: BaseTestContext.() -> Unit = {}
    ): InitSection<Unit, Unit> {
        ActivityTestRule(MainActivity::class.java, false, false)
            .launchActivity(null)
        enableAllFilesAccess()

        //currentData = userDataType?.let { environment.configuration?.get(it) }

        return before(actions = before).after(after)
    }

    protected fun launchIntent(
        //userDataType: UserDataType? = null,
        before: BaseTestContext.() -> Unit = {},
        after: BaseTestContext.() -> Unit = {}
    ): InitSection<Unit, Unit> {
        IntentsTestRule(MainActivity::class.java).launchActivity(Intent())
        enableAllFilesAccess()

        //currentData = userDataType?.let { environment.configuration?.get(it) }

        return before(actions = before).after(after)
    }

    private fun enableAllFilesAccess() {
        val packageName = device.targetContext.packageName
        val message =
            adbServer.performShell("appops get $packageName MANAGE_EXTERNAL_STORAGE allow")[0]
        if (message.contains("deny|default".toRegex())) {
            testLogger.i("Предоставляем приложению доступ ко всем файлам на устройстве")
            adbServer.performShell("appops set $packageName MANAGE_EXTERNAL_STORAGE allow")
        }
    }

    companion object {
        val defaultBuilder
            get() = Kaspresso.Builder.simple().apply {
                flakySafetyParams.timeoutMs = 10_000L
                continuouslyParams.timeoutMs = 1000L

                stepWatcherInterceptors.add(ScreenshotStepInterceptor(EspressoScreenshot()))
                stepWatcherInterceptors.add(AllureMapperStepInterceptor())
            }
    }
}
