package com.escodro.alkaa.kaspresso.screen

import android.view.View
import androidx.fragment.app.Fragment
import com.agoda.kakao.common.views.KView
import com.agoda.kakao.dialog.KAlertDialog
import com.agoda.kakao.image.KImageView
import com.agoda.kakao.recycler.KRecyclerItem
import com.agoda.kakao.recycler.KRecyclerView
import com.agoda.kakao.text.KButton
import com.agoda.kakao.text.KTextView
import com.escodro.alkaa.R
import com.escodro.alkaa.kaspresso.base.BaseScreen
import com.escodro.alkaa.kaspresso.elements.KNavToolbar
import org.hamcrest.Matcher
import kotlin.reflect.KClass

object CategoriesScreen : BaseScreen<CategoriesScreen>() {
    override val layout: Int
        get() = R.layout.fragment_category_list
    override val fragment: KClass<out Fragment>
        get() = TODO("Not yet implemented")

    val mainToolbar = KNavToolbar { withId(R.id.toolbar_main_toolbar) }

    val categoryListEmpty = KTextView { withId(R.id.textview_categorylist_empty) }

    val categoryAddButton = KButton { withId(R.id.button_categorylist_add) }

    val recyclerViewCategoryList = KRecyclerView(
        builder = { withId(R.id.recyclerview_categorylist_list) },
        itemTypeBuilder = {
            itemType(::CategoryItem)
        }
    )

    val editCategoryItem = KView { withText(R.string.category_list_menu_edit) }
    val removeCategoryItem = KView { withText(R.string.category_list_menu_remove) }

    val removeDialog = KAlertDialog()

    /**
     * Метод, который возвращает кнопку категории из Navigation Draw по названию категории
     *
     * @param [categoryName] название категории, по которому необходимо вернуть кнопку
     */
    fun getCategory(categoryName: String): KButton {
        return KButton { withText(categoryName) }
    }

    class CategoryItem(matcher: Matcher<View>) : KRecyclerItem<CategoryItem>(matcher) {
        val itemCategoryColor = KView(matcher) {
            withId(R.id.shapeview_itemcategory_color)
        }
        val itemCategoryDescription = KTextView(matcher) {
            withId(R.id.textview_itemcategory_description)
        }
        val itemCategoryOptions = KImageView(matcher) {
            withId(R.id.imageview_itemcategory_options)
        }
    }
}
