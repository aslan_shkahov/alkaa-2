package com.escodro.alkaa.kaspresso.test

import android.app.Instrumentation
import android.content.Intent
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.Intents.intending
import androidx.test.espresso.intent.matcher.IntentMatchers.hasAction
import androidx.test.espresso.intent.matcher.IntentMatchers.hasData
import com.agoda.kakao.common.utilities.getResourceString
import com.escodro.alkaa.R
import com.escodro.alkaa.kaspresso.base.BaseTestCase
import com.escodro.alkaa.kaspresso.scenario.MoveBetweenTabsScenario
import com.escodro.alkaa.kaspresso.screen.AboutScreen
import com.escodro.alkaa.kaspresso.screen.PreferencesScreen
import com.escodro.alkaa.kaspresso.uiScreen.ChromeUiScreen
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.junit.Test

class CheckGithubProfile : BaseTestCase() {

    companion object {
        private const val PROJECT_URL = "github.com/igorescodro/alkaa"
        private const val HTTPS_PROJECT_URL = "https://github.com/igorescodro/alkaa"
    }

    /**
     * Тест-кейс с проверкой возможности открытия ссылки на GitHub,
     * который использует только KUIautomator
     */
    @Test
    fun checkGithubProfileUsingKUIautomator() = launch().run {
        scenario(
            MoveBetweenTabsScenario(
                getResourceString(R.string.drawer_menu_preferences)
            )
        )

        step("Нажимаем на кнопку About для перехода на соответствующий экран") {
            PreferencesScreen {
                recyclerView.firstChild<PreferencesScreen.PreferencesItem> {
                    itemTextView.click()
                }
            }
        }

        step("Нажимаем на кнопку Visit Project On Github на экране About") {
            AboutScreen {
                githubButton.click()
            }
        }

        ChromeUiScreen {
            compose {
                or(okButton) {
                    isClickable()
                } then {
                    step("Нажимаем на кнопку с текстом ACCEPT & CONTINUE") {
                        okButton.click()
                    }

                    step("Отказываем от авториазции в аккаунте Google") {
                        negativeButton.click()
                    }
                }
                or(urlBar) {
                    isDisplayed()
                } then {
                    testLogger.i(
                        "checkGithubProfileUsingKUIautomator",
                        "urlBar отображается на экране"
                    )
                }
            }

            step(
                "Проверяем что адрес в адресной" +
                        " строке соответствует $PROJECT_URL"
            ) {
                urlBar.hasText(PROJECT_URL)
            }
        }
    }

    /**
     * Тест-кейс с проверкой возможности открытия ссылки на GitHub,
     * который использует для проверки Espresso Intents
     */
    @Test
    fun checkGithubProfileUsingIntent() = launchIntent().run {
        scenario(
            MoveBetweenTabsScenario(
                getResourceString(R.string.drawer_menu_preferences)
            )
        )

        step("Нажимаем на кнопку About для перехода на соответствующий экран") {
            PreferencesScreen {
                recyclerView.firstChild<PreferencesScreen.PreferencesItem> {
                    itemTextView.click()
                }
            }
        }

        var expectedIntent: Matcher<Intent>? = null
        step("Создаём expectedIntent") {
            expectedIntent = allOf(hasAction(Intent.ACTION_VIEW), hasData(HTTPS_PROJECT_URL))
            val result = Instrumentation.ActivityResult(0, null)
            intending(expectedIntent).respondWith(result)
        }

        step("Нажимаем на кнопку Visit Project On Github на экране About") {
            AboutScreen {
                githubButton.click()
            }
        }

        step("Отлавливаем Intent и сверяем его с expectedIntent") {
            intended(expectedIntent)
        }
    }
}
