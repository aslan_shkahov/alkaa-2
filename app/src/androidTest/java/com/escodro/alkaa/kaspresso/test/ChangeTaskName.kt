package com.escodro.alkaa.kaspresso.test

import com.escodro.alkaa.kaspresso.base.BaseTestCase
import com.escodro.alkaa.kaspresso.scenario.CheckTaskScenario
import com.escodro.alkaa.kaspresso.scenario.CreateTaskScenario
import com.escodro.alkaa.kaspresso.screen.TaskDetailScreen
import com.escodro.alkaa.kaspresso.screen.TaskListScreen
import org.junit.Test

class ChangeTaskName : BaseTestCase() {

    /**
     * Тест-кейс, который проверяет возможность переименовать задачу через основной экран
     */
    @Test
    fun changeTaskNameTest() = launch().run {
        val taskName = "Hello1.0"
        val newTaskName = "Hello2.0"

        scenario(CreateTaskScenario(taskName))

        step("Нажимаем на созданную задачу '$taskName'") {
            TaskListScreen {
                taskRecyclerView.firstChild<TaskListScreen.TaskItem> {
                    itemTaskDescription.click()
                }
            }
        }

        step(
            "Меняем название задачи '$taskName' на '$newTaskName'," +
                    " на экране с подробной информацией"
        ) {
            TaskDetailScreen {
                taskDetailTitle.replaceText(newTaskName)
            }
        }

        step("Возвращаемся обратно на экран AllTasks") {
            TaskDetailScreen {
                mainToolbar.navigationButton.click()
            }
        }

        scenario(CheckTaskScenario(newTaskName))
    }
}
