package com.escodro.alkaa.kaspresso.screen

import androidx.fragment.app.Fragment
import com.agoda.kakao.check.KCheckBox
import com.agoda.kakao.edit.KEditText
import com.agoda.kakao.text.KButton
import com.escodro.alkaa.R
import com.escodro.alkaa.kaspresso.base.BaseScreen
import com.escodro.alkaa.kaspresso.elements.KNavToolbar
import kotlin.reflect.KClass

object CategoryScreen : BaseScreen<CategoryScreen>() {
    override val layout: Int
        get() = R.layout.fragment_category_detail
    override val fragment: KClass<out Fragment>
        get() = TODO("Not yet implemented")

    val mainToolbar = KNavToolbar { withId(R.id.toolbar_main_toolbar) }

    val categoryEditText = KEditText { withId(R.id.edittext_categorynew_description) }
    val addCategoryButton = KButton { withId(R.id.button_categorynew_add) }

    //radiogroup_categorynew_label

    val blueRadioButton = KCheckBox { withId(R.id.radiobutton_categorynew_blue) }
    val greenRadioButton = KCheckBox { withId(R.id.radiobutton_categorynew_green) }
    val purpleRadioButton = KCheckBox { withId(R.id.radiobutton_categorynew_purple) }
    val pinkRadioButton = KCheckBox { withId(R.id.radiobutton_categorynew_pink) }
    val redRadioButton = KCheckBox { withId(R.id.radiobutton_categorynew_red) }
    val orangeRadioButton = KCheckBox { withId(R.id.radiobutton_categorynew_orange) }
    val yellowRadioButton = KCheckBox { withId(R.id.radiobutton_categorynew_yellow) }
}