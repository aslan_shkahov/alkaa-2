package com.escodro.alkaa.kaspresso.scenario

import com.agoda.kakao.check.KCheckBox
import com.agoda.kakao.common.utilities.getResourceString
import com.escodro.alkaa.R
import com.escodro.alkaa.kaspresso.screen.CategoriesScreen
import com.escodro.alkaa.kaspresso.screen.CategoryScreen
import com.kaspersky.kaspresso.testcases.api.scenario.Scenario
import com.kaspersky.kaspresso.testcases.core.testcontext.TestContext

/**
 * Сценарий, который создаёт категорию
 *
 * @param [categoryName] название категории, которую необходимо создать
 * @param [categoryColorButton] кнопка, которая выставляет цвет категории
 */
class CreateCategoryScenario(
    categoryName: String,
    categoryColorButton: KCheckBox
) : Scenario() {

    override val steps: TestContext<Unit>.() -> Unit = {
        scenario(
            MoveBetweenTabsScenario(
                getResourceString(R.string.drawer_menu_manage_categories)
            )
        )

        step("Переходим на экран создания категории") {
            CategoriesScreen.categoryAddButton.click()
        }

        step("Вписываем название категории '$categoryName'") {
            CategoryScreen {
                categoryEditText.replaceText(categoryName)
            }
        }

        step("Выбираем цвет для категории '$categoryName'") {
            categoryColorButton.click()
        }

        step("Нажимаем на кнопку создания категории") {
            CategoryScreen {
                addCategoryButton.click()
            }
        }
    }
}
