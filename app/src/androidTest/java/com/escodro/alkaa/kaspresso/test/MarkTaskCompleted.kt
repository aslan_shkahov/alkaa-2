package com.escodro.alkaa.kaspresso.test

import com.agoda.kakao.common.utilities.getResourceString
import com.escodro.alkaa.R
import com.escodro.alkaa.kaspresso.base.BaseTestCase
import com.escodro.alkaa.kaspresso.scenario.CheckNoTaskScenario
import com.escodro.alkaa.kaspresso.scenario.CheckTaskScenario
import com.escodro.alkaa.kaspresso.scenario.CreateTaskScenario
import com.escodro.alkaa.kaspresso.scenario.MoveBetweenTabsScenario
import com.escodro.alkaa.kaspresso.screen.TaskDetailScreen
import com.escodro.alkaa.kaspresso.screen.TaskListScreen
import org.junit.Test

class MarkTaskCompleted : BaseTestCase() {

    companion object {
        private const val TASK_NAME = "taskName"
    }

    /**
     * Тест-кейс, который отмечает задачу выполненной
     * через checkBox (квадрат с галочкой) на главном экране
     */
    @Test
    fun markTaskCompletedOnAllTasksScreen() = launch().run {
        scenario(CreateTaskScenario(TASK_NAME))

        step("Нажимаем на задачу '$TASK_NAME'") {
            TaskListScreen {
                taskRecyclerView.firstChild<TaskListScreen.TaskItem> {
                    itemTaskCheckBox.click()
                }
            }
        }

        scenario(CheckNoTaskScenario(TASK_NAME))

        scenario(
            MoveBetweenTabsScenario(
                getResourceString(R.string.drawer_menu_completed_tasks)
            )
        )

        scenario(CheckTaskScenario(TASK_NAME, prevRecyclerViewSize = 0))
    }

    /**
     * Тест-кейс, который отмечает задачу выполненной
     * через экран с детальной информацией
     */
    @Test
    fun markTaskCompletedOnDetailScreen() = launch().run {
        scenario(CreateTaskScenario(TASK_NAME))

        step("Нажимаем на задачу '$TASK_NAME'") {
            TaskListScreen {
                taskRecyclerView.firstChild<TaskListScreen.TaskItem> {
                    itemTaskDescription.click()
                }
            }
        }

        step(
            "Нажимаем на галочку на экране" +
                    " с детальной информацией о задаче '$TASK_NAME'"
        ) {
            TaskDetailScreen {
                mainToolbar.doneButton.click()
            }
        }

        step(
            "Проверяем отображение всплывающего окна," +
                    " которое сообщает о выполнении задачи '$TASK_NAME'"
        ) {
            TaskDetailScreen {
                checkToast(R.string.task_status_mark_completed)
            }
        }

        step("Возвращаемся на AllTasks экран") {
            TaskDetailScreen {
                mainToolbar.navigationButton.click()
            }
        }

        scenario(CheckNoTaskScenario(TASK_NAME))

        scenario(
            MoveBetweenTabsScenario(
                getResourceString(R.string.drawer_menu_completed_tasks)
            )
        )

        scenario(CheckTaskScenario(TASK_NAME, prevRecyclerViewSize = 0))
    }
}
