package com.escodro.alkaa.kaspresso.test

import androidx.test.espresso.matcher.ViewMatchers.hasErrorText
import com.agoda.kakao.common.utilities.getResourceString
import com.escodro.alkaa.R
import com.escodro.alkaa.kaspresso.base.BaseTestCase
import com.escodro.alkaa.kaspresso.scenario.CreateCategoryScenario
import com.escodro.alkaa.kaspresso.screen.CategoriesScreen
import com.escodro.alkaa.kaspresso.screen.CategoryScreen
import org.junit.Test

class CreateCategory : BaseTestCase() {

    /**
     * Тест-кейс с созданием категории с названием содержащим специальные символами
     */
    @Test
    fun categoryWithSpecialCharacters() = launch().run {
        val categoryName = "{123?.<>$/\\=+*}😀"

        scenario(
            CreateCategoryScenario(
                categoryName,
                CategoryScreen.blueRadioButton
            )
        )

        step(
            "Проверяем наличие категории '$categoryName' в списке категорий"
        ) {
            CategoriesScreen {
                recyclerViewCategoryList.lastChild<CategoriesScreen.CategoryItem> {
                    itemCategoryDescription {
                        isDisplayed()
                        hasText(categoryName)
                    }
                }
            }
        }
    }

    /**
     * Тест-кейс с созданием категории с названием состоящем из пустой строки
     */
    @Test
    fun emptyCategory() = launch().run {
        val categoryName = ""

        scenario(
            CreateCategoryScenario(
                categoryName,
                CategoryScreen.blueRadioButton
            )
        )

        step(
            "Проверяем отображение сообщения об ошибке" +
                    " о пустом поле у поля ввода"
        ) {
            CategoryScreen {
                categoryEditText {
                    matches {
                        hasErrorText(getResourceString(R.string.category_detail_error_empty))
                    }
                }
            }
        }

        step("Возвращаемся на экран со списком категорий") {
            CategoryScreen {
                mainToolbar.navigationButton.click()
            }
        }

        step(
            "Проверяем отсутствие категории '$categoryName' в списке категорий"
        ) {
            CategoriesScreen {
                recyclerViewCategoryList.lastChild<CategoriesScreen.CategoryItem> {
                    itemCategoryDescription {
                        hasNoText(categoryName)
                    }
                }
            }
        }
    }

    /**
     * Тест-кейс с созданием категории с названием содержащим только пробел
     */
    @Test
    fun whiteSpaceCategory() = launch().run {
        val categoryName = " "

        scenario(
            CreateCategoryScenario(
                categoryName,
                CategoryScreen.blueRadioButton
            )
        )

        step(
            "Проверяем отображение сообщения об ошибке" +
                    " о пустом поле у поля ввода"
        ) {
            CategoryScreen {
                categoryEditText {
                    matches {
                        hasErrorText(getResourceString(R.string.category_detail_error_empty))
                    }
                }
            }
        }

        step("Возвращаемся на экран со списком категорий") {
            CategoryScreen {
                mainToolbar.navigationButton.click()
            }
        }

        step(
            "Проверяем отсутствие категории '$categoryName' в списке категорий"
        ) {
            CategoriesScreen {
                recyclerViewCategoryList.lastChild<CategoriesScreen.CategoryItem> {
                    itemCategoryDescription {
                        hasNoText(categoryName)
                    }
                }
            }
        }
    }
}
