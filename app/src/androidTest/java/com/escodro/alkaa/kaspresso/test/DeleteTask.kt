package com.escodro.alkaa.kaspresso.test

import com.escodro.alkaa.kaspresso.base.BaseTestCase
import com.escodro.alkaa.kaspresso.scenario.CheckNoTaskScenario
import com.escodro.alkaa.kaspresso.scenario.CreateTaskScenario
import com.escodro.alkaa.kaspresso.screen.TaskListScreen
import org.junit.Test

class DeleteTask : BaseTestCase() {

    /**
     * Тест-кейс, который проверяет возможность удаления задачи
     */
    @Test
    fun removeTaskTest() = launch().run {
        val taskName = "taskName"

        scenario(CreateTaskScenario(taskName))

        step("Долго нажимаем на созданную задачу '$taskName'") {
            TaskListScreen {
                taskRecyclerView.firstChild<TaskListScreen.TaskItem> {
                    itemTaskDescription.longClick()
                }
            }
        }

        step("Нажимаем на кнопку удаления задачи '$taskName'") {
            TaskListScreen
                .getRemoveButton(device.targetContext.resources)
                .click()
        }

        scenario(CheckNoTaskScenario(taskName))
    }
}
