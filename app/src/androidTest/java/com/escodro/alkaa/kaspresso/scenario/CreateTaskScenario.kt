package com.escodro.alkaa.kaspresso.scenario

import com.escodro.alkaa.kaspresso.screen.TaskListScreen
import com.kaspersky.kaspresso.testcases.api.scenario.Scenario
import com.kaspersky.kaspresso.testcases.core.testcontext.TestContext

/**
 * Сценарий по созданию задачи через главный экран
 *
 * @param [taskName] название задачи, которую необходимо создать
 */
class CreateTaskScenario(
    taskName: String
) : Scenario() {

    override val steps: TestContext<Unit>.() -> Unit = {
        step("Создаём задачу с названием $taskName") {
            TaskListScreen {
                taskRecyclerView.firstChild<TaskListScreen.AddItem> {
                    taskEditText {
                        replaceText(taskName)
                        pressImeAction()
                    }
                }
            }
        }
    }
}
