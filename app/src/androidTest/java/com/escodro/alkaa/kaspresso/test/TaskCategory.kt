package com.escodro.alkaa.kaspresso.test

import com.agoda.kakao.common.utilities.getResourceString
import com.escodro.alkaa.R
import com.escodro.alkaa.kaspresso.base.BaseTestCase
import com.escodro.alkaa.kaspresso.scenario.CheckNoTaskScenario
import com.escodro.alkaa.kaspresso.scenario.CheckTaskScenario
import com.escodro.alkaa.kaspresso.scenario.CreateTaskScenario
import com.escodro.alkaa.kaspresso.scenario.MoveBetweenTabsScenario
import com.escodro.alkaa.kaspresso.scenario.PressFirstTaskCategoryScenario
import com.escodro.alkaa.kaspresso.screen.TaskDetailScreen
import com.escodro.alkaa.kaspresso.screen.TaskListScreen
import org.junit.Test

class TaskCategory : BaseTestCase() {

    companion object {
        private const val TASK_NAME = "taskName"
    }

    /**
     * Тест-кейс с добавлением задачи в категорию Personal
     */
    @Test
    fun setTaskCategory() = launch().run {
        scenario(CreateTaskScenario(TASK_NAME))

        scenario(
            PressFirstTaskCategoryScenario(
                getResourceString(R.string.category_default_personal)
            )
        )

        scenario(CheckTaskScenario(TASK_NAME))

        scenario(
            MoveBetweenTabsScenario(
                getResourceString(R.string.category_default_personal)
            )
        )

        scenario(CheckTaskScenario(TASK_NAME))
    }

    /**
     * Тест-кейс с добавлением и удалением задачи из категории Personal
     */
    @Test
    fun deleteTaskFormCategory() = launch().run {
        scenario(CreateTaskScenario(TASK_NAME))

        scenario(
            PressFirstTaskCategoryScenario(
                getResourceString(R.string.category_default_personal)
            )
        )

        scenario(
            MoveBetweenTabsScenario(
                getResourceString(R.string.category_default_personal)
            )
        )

        scenario(CheckTaskScenario(TASK_NAME))

        scenario(
            PressFirstTaskCategoryScenario(
                getResourceString(R.string.category_default_personal)
            )
        )

        scenario(CheckNoTaskScenario(TASK_NAME))

        scenario(
            MoveBetweenTabsScenario(
                getResourceString(R.string.drawer_menu_all_tasks)
            )
        )

        scenario(CheckTaskScenario(TASK_NAME))
    }

    /**
     * Тест-кейс, который проверяет возможность изменения категории у задачи
     */
    @Test
    fun changeTaskCategory() = launch().run {
        scenario(CreateTaskScenario(TASK_NAME))

        step("Нажимаем на задачу '$TASK_NAME'") {
            TaskListScreen {
                taskRecyclerView.firstChild<TaskListScreen.TaskItem> {
                    itemTaskDescription.click()
                }
            }
        }

        step(
            "Добавляем задачу '$TASK_NAME' в категорию Personal"
        ) {
            TaskDetailScreen {
                categoryChipGroup.selectChip(
                    getResourceString(R.string.category_default_personal)
                )
            }
        }

        step("Меняем категорию у задачи '$TASK_NAME' с Personal на Work") {
            TaskDetailScreen {
                categoryChipGroup.selectChip(
                    getResourceString(R.string.category_default_work)
                )
            }
        }

        step("Возвращаемся на экран AllTasks") {
            TaskDetailScreen {
                mainToolbar.navigationButton.click()
            }
        }

        scenario(CheckTaskScenario(TASK_NAME))

        scenario(
            MoveBetweenTabsScenario(
                getResourceString(R.string.category_default_personal)
            )
        )

        scenario(CheckNoTaskScenario(TASK_NAME))

        scenario(
            MoveBetweenTabsScenario(
                getResourceString(R.string.category_default_work)
            )
        )

        scenario(CheckTaskScenario(TASK_NAME))
    }
}
