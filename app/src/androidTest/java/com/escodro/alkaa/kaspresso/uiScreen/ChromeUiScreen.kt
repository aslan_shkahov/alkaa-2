package com.escodro.alkaa.kaspresso.uiScreen

import com.kaspersky.components.kautomator.component.edit.UiEditText
import com.kaspersky.components.kautomator.component.text.UiButton
import com.kaspersky.components.kautomator.screen.UiScreen

object ChromeUiScreen : UiScreen<ChromeUiScreen>() {
    override val packageName: String
        get() = TODO("Not yet implemented")

    val okButton = UiButton {
        withId(this@ChromeUiScreen.CHROME_PACKAGE, "terms_accept")
    }

    private const val CHROME_PACKAGE = "com.android.chrome"

    val negativeButton = UiButton {
        withId(this@ChromeUiScreen.CHROME_PACKAGE, "negative_button")
    }

    val urlBar = UiEditText {
        withId(this@ChromeUiScreen.CHROME_PACKAGE, "url_bar")
    }
}
