package com.escodro.alkaa.kaspresso.test

import androidx.annotation.ColorRes
import com.agoda.kakao.check.KCheckBox
import com.agoda.kakao.common.utilities.getResourceString
import com.escodro.alkaa.R
import com.escodro.alkaa.kaspresso.base.BaseTestCase
import com.escodro.alkaa.kaspresso.extension.checkTint
import com.escodro.alkaa.kaspresso.scenario.MoveBetweenTabsScenario
import com.escodro.alkaa.kaspresso.screen.CategoriesScreen
import com.escodro.alkaa.kaspresso.screen.CategoryScreen
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

/**
 * Класс содержащий тесты для проверки изменения интерфейса
 * при выборе цвета будущей категории
 *
 * @param [tintColorId] id цвета, с которым необходимо сравнить цвет интерфейса
 * @param [radioButton] кнопка, на которую необходимо нажать,
 * чтобы выбрать цвет интерфейса
 * @param [radioButtonColorName] название цвета для его вывода при выполнении step'а
 */
@RunWith(Parameterized::class)
class CheckCategoriesColors(
    @ColorRes private val tintColorId: Int,
    private val radioButton: KCheckBox,
    private val radioButtonColorName: String
) : BaseTestCase() {

    companion object {

        @JvmStatic
        @Parameterized.Parameters
        fun data(): Array<Array<Any>> {
            return arrayOf(
                arrayOf(R.color.blue, CategoryScreen.blueRadioButton, "синий"),
                arrayOf(R.color.green, CategoryScreen.greenRadioButton, "зелёный"),
                arrayOf(R.color.purple, CategoryScreen.purpleRadioButton, "фиолетовый"),
                arrayOf(R.color.pink, CategoryScreen.pinkRadioButton, "розовый"),
                arrayOf(R.color.red, CategoryScreen.redRadioButton, "красный"),
                arrayOf(R.color.orange, CategoryScreen.orangeRadioButton, "оранжевый"),
                arrayOf(R.color.yellow, CategoryScreen.yellowRadioButton, "жёлтый")
            )
        }
    }

    /**
     * Тест-кейс с проверкой правильного изменения цвета
     * интерфейса, при выборе соответствующего цвета
     * при создании задачи
     */
    @Test
    fun checkCategoriesColorsTest() = launch().run {
        scenario(
            MoveBetweenTabsScenario(
                getResourceString(R.string.drawer_menu_manage_categories)
            )
        )

        step("Переходим на экран создания категории") {
            CategoriesScreen {
                categoryAddButton.click()
            }
        }

        step("Скрываем клавиатуру") {
            device.uiDevice.pressBack()
        }

        step("Выставляем $radioButtonColorName цвет категории") {
            if (radioButton == CategoryScreen.blueRadioButton) {
                CategoryScreen {
                    greenRadioButton.click()
                }
            }
            radioButton.click()
        }

        step(
            "Проверяем, что линия под полем ввода" +
                    " соответствует цвету tintColorId"
        ) {
            CategoryScreen {
                categoryEditText.checkTint(tintColorId)
            }
        }

        step(
            "Проверяем, что цвет кнопки выбора цвета" +
                    " соответствует заданному цвету tintColorId"
        ) {
            radioButton.checkTint(tintColorId)
        }

        step(
            "Проверяем, что цвет кнопки создания категории" +
                    " соответствует заданному цвету tintColorId"
        ) {
            CategoryScreen {
                addCategoryButton.hasBackgroundColor(tintColorId)
            }
        }
    }
}
