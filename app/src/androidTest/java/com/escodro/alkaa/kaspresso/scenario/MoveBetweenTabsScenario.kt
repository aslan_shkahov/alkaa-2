package com.escodro.alkaa.kaspresso.scenario

import com.escodro.alkaa.kaspresso.screen.TaskListScreen
import com.kaspersky.kaspresso.testcases.api.scenario.Scenario
import com.kaspersky.kaspresso.testcases.core.testcontext.TestContext

/**
 * Сценарий, который позволяет перемещаться между вкладками,
 * передавая название вкладки
 *
 * @param [tabName] название вкладки, на которую необходимо переместиться
 */
class MoveBetweenTabsScenario(
    tabName: String
) : Scenario() {

    override val steps: TestContext<Unit>.() -> Unit = {
        step("Открываем Navigation Drawer") {
            TaskListScreen {
                mainToolbar.navigationButton.click()
            }
        }

        step("Переходим на экран $tabName") {
            TaskListScreen.getCategory(tabName).click()
        }
    }
}
