package com.escodro.alkaa.kaspresso.elements

import android.widget.ImageButton
import com.agoda.kakao.common.builders.ViewBuilder
import com.agoda.kakao.common.views.KBaseView
import com.agoda.kakao.image.KImageView
import com.agoda.kakao.toolbar.ToolbarViewActions
import com.agoda.kakao.toolbar.ToolbarViewAssertions
import com.escodro.alkaa.R

/**
 * Кастомный класс для взаимодействия с KToolbar, отличается
 * от базового тема что есть переменные для кнопки навигации
 * (левый верхний угол) и кнопки выполненной задачи / поиска задачи
 * (правый верхний угол)
 */
class KNavToolbar(function: ViewBuilder.() -> Unit) : KBaseView<KNavToolbar>(function),
    ToolbarViewActions, ToolbarViewAssertions {
    val navigationButton: KImageView
    val doneButton: KImageView

    init {
        navigationButton = KImageView {
            isDescendantOfA(function)
            isAssignableFrom(ImageButton::class.java)
        }
        doneButton = KImageView {
            withId(R.id.key_update_completed_status)
        }
    }
}
