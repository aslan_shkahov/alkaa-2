package com.escodro.alkaa.kaspresso.scenario

import com.escodro.alkaa.kaspresso.screen.TaskListScreen
import com.kaspersky.kaspresso.testcases.api.scenario.Scenario
import com.kaspersky.kaspresso.testcases.core.testcontext.TestContext
import org.junit.Assert.assertTrue

/**
 * Сценарий по проверке наличия задачи с указанным названием
 *
 * @param [taskName] название задачи, наличие которой необходимо проверить
 * @param [prevRecyclerViewSize] количество элементов в recyclerView до
 * возможного добавления элемента
 */
class CheckTaskScenario(
    taskName: String,
    prevRecyclerViewSize: Int = 1
) : Scenario() {

    override val steps: TestContext<Unit>.() -> Unit = {
        step("Проверяем наличие задачи '$taskName'") {
            TaskListScreen {
                assertTrue(
                    "Размер списка с задачами не увеличился" +
                            " по сравнения с предыдущим размером '$prevRecyclerViewSize'",
                    prevRecyclerViewSize < taskRecyclerView.getSize()
                )

                taskRecyclerView.firstChild<TaskListScreen.TaskItem> {
                    isDisplayed()
                    itemTaskDescription.hasText(taskName)
                }
            }
        }
    }
}
