package com.escodro.alkaa.kaspresso.screen

import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.agoda.kakao.chipgroup.KChipGroup
import com.agoda.kakao.edit.KEditText
import com.agoda.kakao.image.KImageView
import com.agoda.kakao.picker.date.KDatePickerDialog
import com.agoda.kakao.picker.time.KTimePickerDialog
import com.agoda.kakao.scroll.KScrollView
import com.agoda.kakao.text.KButton
import com.agoda.kakao.text.KTextView
import com.escodro.alkaa.R
import com.escodro.alkaa.kaspresso.base.BaseScreen
import com.escodro.alkaa.kaspresso.elements.KNavToolbar
import com.escodro.alkaa.kaspresso.matcher.ToastMatcher
import kotlin.reflect.KClass

object TaskDetailScreen : BaseScreen<TaskDetailScreen>() {
    override val layout: Int
        get() = R.layout.fragment_task_detail
    override val fragment: KClass<out Fragment>
        get() = TODO("Not yet implemented") //TaskDetailFragment

    //Toolbar элементы
    val mainToolbar = KNavToolbar { withId(R.id.toolbar_main_toolbar) }
    val toolbarTitle = KTextView { withId(R.id.toolbar_title) }

    val categoryScrollView = KScrollView { withId(R.id.scrollview_taskdetail_category) }
    val categoryChipGroup = KChipGroup { withId(R.id.chipgrp_taskdetail_category) }

    val taskDetailTitle = KEditText { withId(R.id.edittext_taskdetail_title) }
    val taskDetailDescriptionLogo = KImageView { withId(R.id.imageview_taskdetail_description) }
    val taskDetailDescription = KEditText { withId(R.id.edittext_taskdetail_description) }

    val alarmButton = KButton { withId(R.id.btn_taskdetail_date) }

    val datePickerDialog = KDatePickerDialog()
    val timePickerDialog = KTimePickerDialog()

    /**
     * Метод, который ищет Toast по тексту
     *
     * @param [textId] - id строкового ресурса,
     * по которому будет выполняться поиск
     */
    fun checkToast(@StringRes textId: Int) = KTextView {
        withText(textId)
    } perform {
        inRoot {
            withMatcher(ToastMatcher())
        }
        hasText(textId)
        isDisplayed()
    }
}
