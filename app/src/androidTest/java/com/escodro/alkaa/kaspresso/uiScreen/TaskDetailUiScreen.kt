package com.escodro.alkaa.kaspresso.uiScreen

import com.kaspersky.components.kautomator.component.common.views.UiView
import com.kaspersky.components.kautomator.screen.UiScreen

object TaskDetailUiScreen : UiScreen<TaskDetailUiScreen>() {
    override val packageName: String
        get() = "com.escodro.alkaa"

    val taskDetailTitle = UiView {
        withId(
            this@TaskDetailUiScreen.packageName,
            "edittext_taskdetail_title"
        )
    }
}
