package com.escodro.alkaa.kaspresso.uiScreen

import com.kaspersky.components.kautomator.component.common.views.UiView
import com.kaspersky.components.kautomator.screen.UiScreen

object NotificationUiScreen : UiScreen<NotificationUiScreen>() {
    override val packageName: String
        get() = "com.escodro.alkaa"

    /**
     * Метод, который возвращает уведомление по тексту, который содержится в нём
     *
     * @param [notificationText] текст, который должен содержать в уведомлении
     */
    fun getNotification(notificationText: String): UiView {
        return UiView { withText(notificationText) }
    }
}
