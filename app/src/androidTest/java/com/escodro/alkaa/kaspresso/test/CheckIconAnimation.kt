package com.escodro.alkaa.kaspresso.test

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.test.espresso.Espresso.closeSoftKeyboard
import com.agoda.kakao.check.KCheckBox
import com.agoda.kakao.common.utilities.getResourceString
import com.escodro.alkaa.R
import com.escodro.alkaa.kaspresso.base.BaseTestCase
import com.escodro.alkaa.kaspresso.extension.checkIcon
import com.escodro.alkaa.kaspresso.scenario.MoveBetweenTabsScenario
import com.escodro.alkaa.kaspresso.screen.CategoriesScreen
import com.escodro.alkaa.kaspresso.screen.CategoryScreen
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

/**
 * Класс содержащий тесты для проверки, что при нажатии кнопки выбора цвета категории,
 * иконка у кнопки, меняется на полую или заполненную
 *
 * @param [buttonForCheck] кнопка, изменение которой необходимо проверить
 * @param [buttonForPress] кнопка на которую необходимо нажать
 * @param [buttonForCheckColor] id цвета кнопки, которую необходимо проверить
 * @param [colorName] название цвета, для его вывода при выполнении step'а
 * @param [expectedDrawable] id drawable, который должен быть у buttonForCheck
 */
@RunWith(Parameterized::class)
class CheckIconAnimation(
    private val buttonForCheck: KCheckBox,
    private val buttonForPress: KCheckBox,
    @ColorRes private val buttonForCheckColor: Int,
    private val colorName: String,
    @DrawableRes private val expectedDrawable: Int
) : BaseTestCase() {

    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun data(): Array<Array<Any>> {
            return arrayOf(
                arrayOf(
                    CategoryScreen.blueRadioButton,
                    CategoryScreen.greenRadioButton,
                    R.color.blue,
                    "зелёный",
                    R.drawable.ic_category_label_unchecked
                ),
                arrayOf(
                    CategoryScreen.greenRadioButton,
                    CategoryScreen.purpleRadioButton,
                    R.color.green,
                    "фиолетовый",
                    R.drawable.ic_category_label_unchecked
                ),
                arrayOf(
                    CategoryScreen.purpleRadioButton,
                    CategoryScreen.pinkRadioButton,
                    R.color.purple,
                    "розовый",
                    R.drawable.ic_category_label_unchecked
                ),
                arrayOf(
                    CategoryScreen.pinkRadioButton,
                    CategoryScreen.redRadioButton,
                    R.color.pink,
                    "красный",
                    R.drawable.ic_category_label_unchecked
                ),
                arrayOf(
                    CategoryScreen.redRadioButton,
                    CategoryScreen.orangeRadioButton,
                    R.color.red,
                    "оранжевый",
                    R.drawable.ic_category_label_unchecked
                ),
                arrayOf(
                    CategoryScreen.orangeRadioButton,
                    CategoryScreen.yellowRadioButton,
                    R.color.orange,
                    "жёлтый",
                    R.drawable.ic_category_label_unchecked
                ),
                arrayOf(
                    CategoryScreen.yellowRadioButton,
                    CategoryScreen.blueRadioButton,
                    R.color.yellow,
                    "синий",
                    R.drawable.ic_category_label_unchecked
                ),
                arrayOf(
                    CategoryScreen.blueRadioButton,
                    CategoryScreen.blueRadioButton,
                    R.color.blue,
                    "синий",
                    R.drawable.ic_category_label_checked
                ),
                arrayOf(
                    CategoryScreen.greenRadioButton,
                    CategoryScreen.greenRadioButton,
                    R.color.green,
                    "зелёный",
                    R.drawable.ic_category_label_checked
                ),
                arrayOf(
                    CategoryScreen.purpleRadioButton,
                    CategoryScreen.purpleRadioButton,
                    R.color.purple,
                    "фиолетовый",
                    R.drawable.ic_category_label_checked
                ),
                arrayOf(
                    CategoryScreen.pinkRadioButton,
                    CategoryScreen.pinkRadioButton,
                    R.color.pink,
                    "розовый",
                    R.drawable.ic_category_label_checked
                ),
                arrayOf(
                    CategoryScreen.redRadioButton,
                    CategoryScreen.redRadioButton,
                    R.color.red,
                    "красный",
                    R.drawable.ic_category_label_checked
                ),
                arrayOf(
                    CategoryScreen.orangeRadioButton,
                    CategoryScreen.orangeRadioButton,
                    R.color.orange,
                    "оранжевый",
                    R.drawable.ic_category_label_checked
                ),
                arrayOf(
                    CategoryScreen.yellowRadioButton,
                    CategoryScreen.yellowRadioButton,
                    R.color.yellow,
                    "жёлтый",
                    R.drawable.ic_category_label_checked
                )
            )
        }
    }

    /**
     * Тест-кейс с проверкой заливки иконки после выбора цвета задачи
     */
    @Test
    fun checkIcon() = launch().run {
        scenario(
            MoveBetweenTabsScenario(
                getResourceString(R.string.drawer_menu_manage_categories)
            )
        )

        step("Переходим на экран создания категории") {
            CategoriesScreen {
                categoryAddButton.click()
            }
        }

        step("Скрываем клавиатуру") {
            closeSoftKeyboard()
        }

        step("Выбираем $colorName цвет категории") {
            buttonForPress.click()
        }

        step("Проверяем заполнение иконки") {
            buttonForCheck.checkIcon(
                expectedDrawable,
                buttonForCheckColor
            )
        }
    }
}
