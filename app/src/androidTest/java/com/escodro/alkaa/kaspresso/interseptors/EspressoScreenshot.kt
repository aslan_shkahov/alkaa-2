package com.escodro.alkaa.kaspresso.interseptors

import android.os.Environment
import android.util.Log
import androidx.test.runner.screenshot.Screenshot
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

/**
 * Сохраняет сриншот
 */
class EspressoScreenshot {
    private val screenshotFolder = File("${Environment.getExternalStorageDirectory()}/screenshots")
    private val tag = EspressoScreenshot::class.java.simpleName

    private fun prepareScreenshotPath() {
        try {
            screenshotFolder.mkdirs()
        } catch (ignored: Exception) {
            Log.e(tag, "Failed to make screenshot folder $screenshotFolder")
        }
    }

    /**
     * Делает скриншот, сохраняет на устройстве
     *
     * @param [description] Имя файла скриншота
     * @return [File] Скриншот
     */
    fun takeScreenshot(description: String): File {
        prepareScreenshotPath()

        val capture = Screenshot.capture()

        val imageFile = File(screenshotFolder, "$description.jpg")
        var out: BufferedOutputStream? = null
        try {
            Log.i(tag, "Saving screenshot to " + imageFile.absolutePath)
            out = BufferedOutputStream(FileOutputStream(imageFile))
            capture.bitmap.compress(capture.format, QUALITY, out)
            out.flush()
            Log.i(tag, "Screenshot exists? " + imageFile.exists())
        } catch (ignored: Exception) {
            Log.e(tag, ignored.toString())
            ignored.printStackTrace()
        } finally {
            try {
                out?.close()
            } catch (ignored: IOException) {
                Log.e(tag, ignored.toString())
                ignored.printStackTrace()
            }
        }
        return imageFile
    }

    companion object {
        private const val QUALITY = 100
    }
}