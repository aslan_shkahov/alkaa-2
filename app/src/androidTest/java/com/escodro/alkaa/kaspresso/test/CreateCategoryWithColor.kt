package com.escodro.alkaa.kaspresso.test

import androidx.annotation.ColorRes
import com.agoda.kakao.check.KCheckBox
import com.escodro.alkaa.R
import com.escodro.alkaa.kaspresso.base.BaseTestCase
import com.escodro.alkaa.kaspresso.extension.checkShapeViewColor
import com.escodro.alkaa.kaspresso.scenario.CreateCategoryScenario
import com.escodro.alkaa.kaspresso.screen.CategoriesScreen
import com.escodro.alkaa.kaspresso.screen.CategoryScreen
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

/**
 * Класс содержащий тесты для проверки возможности создания категорий с разными цветами
 *
 * @param [radioButton] кнопка на которую необходимо нажать
 * @param [colorId] id цвета кнопки, которую необходимо проверить
 * чтобы выбрать цвет интерфейса
 */
@RunWith(Parameterized::class)
class CreateCategoryWithColor(
    private val radioButton: KCheckBox,
    @ColorRes private val colorId: Int
) : BaseTestCase() {

    companion object {

        @JvmStatic
        @Parameterized.Parameters
        fun data(): Array<Array<Any>> {
            return arrayOf(
                arrayOf(CategoryScreen.blueRadioButton, R.color.blue),
                arrayOf(CategoryScreen.greenRadioButton, R.color.green),
                arrayOf(CategoryScreen.purpleRadioButton, R.color.purple),
                arrayOf(CategoryScreen.pinkRadioButton, R.color.pink),
                arrayOf(CategoryScreen.redRadioButton, R.color.red),
                arrayOf(CategoryScreen.orangeRadioButton, R.color.orange),
                arrayOf(CategoryScreen.yellowRadioButton, R.color.yellow)
            )
        }
    }

    /**
     * Тест-кейс, который проверяет возможность создать категории с разными цветами
     */
    @Test
    fun createCategoryWithColorTest() = launch().run {
        val categoryName = "categoryName"

        scenario(CreateCategoryScenario(categoryName, radioButton))

        step(
            "Проверяем наличие категории '$categoryName' в списке категорий"
        ) {
            CategoriesScreen {
                recyclerViewCategoryList.lastChild<CategoriesScreen.CategoryItem> {
                    step(
                        "Проверяем соответствие цвета ShapeView" +
                                " на экране со всеми категориями"
                    ) {
                        itemCategoryColor.checkShapeViewColor(colorId)
                    }

                    step("Проверяем отображение названия категории $categoryName") {
                        itemCategoryDescription {
                            isDisplayed()
                            hasText(categoryName)
                        }
                    }
                }
            }
        }
    }
}
