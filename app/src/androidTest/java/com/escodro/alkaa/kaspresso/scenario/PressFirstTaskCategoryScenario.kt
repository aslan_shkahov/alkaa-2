package com.escodro.alkaa.kaspresso.scenario

import com.escodro.alkaa.kaspresso.screen.TaskDetailScreen
import com.escodro.alkaa.kaspresso.screen.TaskListScreen
import com.kaspersky.kaspresso.testcases.api.scenario.Scenario
import com.kaspersky.kaspresso.testcases.core.testcontext.TestContext

/**
 * Сценарий по добавлению первой задачи в категорию
 *
 * @param [categoryName] название категории, в которую необходимо добавить задачу
 */
class PressFirstTaskCategoryScenario(
    categoryName: String
) : Scenario() {

    override val steps: TestContext<Unit>.() -> Unit = {
        step("Нажимаем на задачу") {
            TaskListScreen {
                taskRecyclerView.firstChild<TaskListScreen.TaskItem> {
                    itemTaskDescription.click()
                }
            }
        }

        step("Добавляем задачу в категорию Personal") {
            TaskDetailScreen {
                categoryChipGroup.selectChip(categoryName)
            }
        }

        step("Возвращаемся на экран AllTasks") {
            TaskDetailScreen {
                mainToolbar.navigationButton.click()
            }
        }
    }
}
