package com.escodro.alkaa.kaspresso.matcher

import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.view.View
import androidx.annotation.ColorRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.graphics.drawable.toBitmap
import com.agoda.kakao.common.utilities.getResourceColor
import com.agoda.kakao.common.utilities.getResourceDrawable
import com.escodro.alkaa.R
import org.hamcrest.Description
import org.hamcrest.TypeSafeMatcher

/**
 * Матчер, который проверяет соответствие переданного цвета с тем,
 * который есть на текущий момент у ShapeView
 *
 * @param [expectedTintColorId] id цвета ShapeView, который должен быть у передаваемого ShapeView
 */
class CheckShapeViewColorMatcher(
    @ColorRes private val expectedTintColorId: Int
) : TypeSafeMatcher<View>(View::class.java) {
    override fun describeTo(description: Description?) {
        description?.appendText("Цвет $expectedTintColorId не совпадает с tint ShapeView")
    }

    override fun matchesSafely(item: View?): Boolean {
        if (item == null) return false

        return if (item is AppCompatImageView) {
            val expectDrawable = getResourceDrawable(R.drawable.shape_category_circle)
                ?: return false

            expectDrawable.colorFilter = PorterDuffColorFilter(
                getResourceColor(expectedTintColorId),
                PorterDuff.Mode.MULTIPLY
            )

            val expectedBitmap = expectDrawable.toBitmap()

            val current = item.drawable.toBitmap()
            return current.sameAs(expectedBitmap)

        } else false
    }

}
