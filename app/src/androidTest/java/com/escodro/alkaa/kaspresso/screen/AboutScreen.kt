package com.escodro.alkaa.kaspresso.screen

import androidx.fragment.app.Fragment
import com.agoda.kakao.text.KButton
import com.escodro.alkaa.R
import com.escodro.alkaa.kaspresso.base.BaseScreen
import kotlin.reflect.KClass

object AboutScreen : BaseScreen<AboutScreen>() {
    override val layout: Int
        get() = R.layout.fragment_about
    override val fragment: KClass<out Fragment>
        get() = TODO("Not yet implemented") //AboutFragment

    val githubButton = KButton { withId(R.id.button_about_project) }
}